# Create Test Cases and Run a Series of Test #

**Do NOT** run the executables in this folder without knowing what
you are doing:

`run`, `create-test-cases`, and `plot.R`

**Do NOT** run them or they may overwrite your existing test cases.

Read the block comments in each script to understand what they do
before running them.
