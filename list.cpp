/* Project 5,  Sorting Algorithms
 * CS 5610
 * Pooya Taherkhani,  pt376511@ohio.edu
 * Sun May 14 17:38:11 EDT 2017
 */

#include <iostream>
#include "list.h"

using namespace std;

/* ===========================================================================
 * input:  read the count and all integers into the list from standard input
 * =========================================================================== */
void List::input(int& count)
{
  int num;
  cin >> count;
  for (int i = 0; i < count; ++i) {
    cin >> num;
    array.push_back(num);
  }
}

/* ===========================================================================
 * output:  print all integers in the list to standard output 
 * =========================================================================== */
void List::output()
{
  vector<int>::iterator itr;
  for (itr = array.begin(); itr != array.end(); ++itr) {
    cout << *itr << ' ';
  }
  cout << endl;
}

/* ===========================================================================
 * quicksort:  perform quicksort on array with the desired pivot and return
 *             number of key comparisons
 * take in:    p and q: start index and end index of array
 *             p_t:     a character indicating pivot type:
 *                      f (or any character other than l and m): first element,
 *                      l: last element,
 *                      m: median of first, last, and middle elements
 * =========================================================================== */
void List::quicksort(int p, int q, char p_t, unsigned long long int& comp_count)
{
  if (p < q) {
    int m; 			// pivot index after partition is performed
    switch (p_t) {
    case 'l':
      m = partition_last(p, q, comp_count);
      break;
    case 'm':
      m = partition_median(p, q, comp_count);
      break;
    default:
      m = partition_first(p, q, comp_count);
    }
    quicksort(p, m-1, p_t, comp_count);
    quicksort(m+1, q, p_t, comp_count);
  }
}

/* ===========================================================================
 * partition_first:  partition array using first element as pivot into two
 *                   segments, left segment: less-than-or-equal-to pivot, right
 *                   segment: greater-than pivot
 * take in:          p and q: start index and end index of array
 * return:           greatest index of the less-than-or-equal segment; and
 *                   comp_count: number of key comparisons
 * call:             swap
 * (based on algorithm provided in CLRS textbook)
 * =========================================================================== */
int List::partition_first(int p, int q, unsigned long long int& comp_count)
{
  int pivot = array[p];
  int i = p;
  for (int j = p+1; j < q+1; ++j) {
    // at least one and at most two comparisons, but we count it as one
    ++comp_count;
    if (array[j] <= pivot) {
      ++i;
      swap(array[i], array[j]);
    }
  }
  swap(array[i], array[p]);
  return i;
}

/* ===========================================================================
 * swap:  swap two integers
 * =========================================================================== */
void List::swap(int& a, int& b)
{
  int temp = a;
  a = b;
  b = temp;
}

/* ===========================================================================
 * partition_last:  partition array using last element as pivot
 * (similar to partition_first)
 * (exactly same as algorithm provided in CLRS textbook)
 * =========================================================================== */
int List::partition_last(int p, int q, unsigned long long int& comp_count)
{
  int pivot = array[q];
  int i = p-1;
  for (int j = p; j < q; ++j) {
    ++comp_count;
    if (array[j] <= pivot) {
      ++i;
      swap(array[i], array[j]);
    }
  }
  swap(array[i+1], array[q]);
  return i+1;
}

/* ===========================================================================
 * partition_median:  partition array using median of first, last, and middle
 *                    element as pivot
 * call:              swap, partition_first
 * (similar to partition_first)
 * =========================================================================== */
int List::partition_median(int p, int q, unsigned long long int& comp_count)
{
  // compute index of middle element
  // compute index of median
  // swap median with first element
  // perform partition_first
  int middle_idx = p + (q - p)/2; // our definition of middle index
  int med_idx = median_idx(p, q, middle_idx, comp_count);
  swap(array[p], array[med_idx]);
  return partition_first(p, q, comp_count);	// return index of pivot
}

/* ===========================================================================
 * median_idx:  take in three indices of array and return the index of the median
 *              of three array elements and the number of key comparisons
 * =========================================================================== */
int List::median_idx(int i, int j, int k, unsigned long long int& comp_count)
{
  if ((array[j] <= array[i] && array[i] <= array[k]) ||
      (array[k] <= array[i] && array[i] <= array[j])) { // first if clause
    comp_count += 2; // two comparisons needed for condition above to be true
    return i;  
  } else if ((array[i] <= array[j] && array[j] <= array[k]) ||
	     (array[k] <= array[j] && array[j] <= array[i])) { // second if clause
    // at least four comparisons for first if clause to be false, and
    // two comparisons for second if clause to be true
    comp_count += 6;
    return j;  
  } else {
    // at least four comparisons for each of first and second if clauses to be false
    comp_count += 8;
    return k;  
  }
}

/* ===========================================================================
 * insertion sort:  sort the array and return number of key comparisons
 * (based on CLRS textbook)
 * =========================================================================== */
unsigned long long int List::insertion_sort(int int_count)
{
  // cout << "Start insertion sort\n";
  unsigned long long int comp_count = 0,
                         move_count = 0;
  for (int i = 1; i < int_count; ++i) { // start key with second element of array
    // if (i % 1000 == 0)
    //   cout << i << ' ';
    int key = array[i],
        j = i-1;
    while (j > -1 && key < array[j]) { // check key against all elements to its left
      ++comp_count;
      array[j+1] = array[j];	// move left element to right (if greater than key)
      ++move_count;
      --j;			// do the same for next left element
    }
    if (j > -1)
      ++comp_count;
    array[j+1] = key;		// insert key where it belongs!
    if (j != i-1)
      ++move_count;
  }
  // cout << ',' << move_count;

  return comp_count;
}

/* ===========================================================================
 * shellsort:  sort the array and return number of key comparisons
 * =========================================================================== */
unsigned long long int List::shellsort(int int_count)
{
  unsigned long long int comp_count = 0,
                         move_count = 0; // number of key movements

  //** generate increment sequence
  vector<int> inc_seq;
  // cout //<< "increment sequence:\n"
  //      << "H = { ";

  //** use a different increment sequence depending on array length
  //** conditioning based on Knuth, TAOCP, volume 3, page 95

  // if (int_count < 1000) { //*******************************
    for (int inc = 1; inc < (int_count-1)/9 + 2; inc = 3*inc + 1) {
      inc_seq.push_back(inc);
      // cout << inc << ' '; 
    }
    // cout << "}\n";
    // cout << int_count << " < 1000\n";

  // } else {
  //   for (int inc = 1; inc < (int_count-1)/4; inc = int(2.25*inc) + 1) {
  //     inc_seq.push_back(inc);
  //     // cout << inc << ' '; 
  //   }
  //   // cout << "}\n";
  //   // cout << int_count << " > 1000\n";
  // }

  //** sort the shell out of the array!
  vector<int>::reverse_iterator itr;	// to iterate inc_seq in reverse
  for (itr = inc_seq.rbegin(); itr != inc_seq.rend(); ++itr) {
    int h = *itr;
    int m = 0;
    while (m < h) {

      //** insertion sort for subarray starting at index m with consecutive
      //** elements h units away from each other in the original array
      //** look at insertion_sort for further explanation
      for (int i = m+h; i < int_count; i += h) {
        int key = array[i],
        j = i-h;
        while (j > m-1 && key < array[j]) {
          ++comp_count;
          array[j+h] = array[j];
          ++move_count;
          j -= h;
        }
        if (j > m-1)
          ++comp_count;
        array[j+h] = key;
        if (j != i-h)
          ++move_count;
      }

      m += 1;
    }
  }
  // cout << ',' << move_count;

  return comp_count;
}
