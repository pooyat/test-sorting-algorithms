# Test sorting algorithms #

This program allows for testing quicksort, Shellsort, and insertion
sort algorithms.  The program outputs the number of comparisons to
sort a sequence of integers.

For analysis, test results, and discussion look at the PDF report: REPORT.pdf

## Run a Single Test ##

To run the program, type the following at the command line.

```bash
$ make
$ ./project5 < input_file
```

The input file contains an array of integers in the follwoing format.

```
n a_1 a_2 ... a_n
```

where `n` represents the number of elements in the input file and
`a_i` is the `i_th` integer in the input array.

The program then outputs in the following format.

```
n,number_of_comparisons
```

that is, the array size `n` and the number of comparisons performed.

### Example ###

So for instance:

```bash
$ ./project5 < testcases/quicksort-worst-i-0001.in
1000,431157
```

which means it took 431157 comparisons to sort the given 1000 element
array using the default sorting algorithm (which is quicksort).

### Options ###

You can also use the following options to pick which sorting algorithm
to use.

Options:

   `-s`  performs shellsort on input array then displays results

   `-i`  performs insertion sort on input array then displays results

   `-l`  performs quicksort on input array, using the last element
         as the pivot, then displays result

   `-m`  performs quicksort on input array, using the median of
         the first, middle and last elements as the pivot, then
         displays results

[no option]  by default, performs quicksort on input array, using
             the first element as the pivot, then displays result

[more than one option]  only the first option will take effect

### Example ###

For instace:

```bash
$ ./project5 -s < testcases/quicksort-worst-i-0001.in   
1000,3942
```

which means using Shellsort it took only 3942 comparisons to sort the
same 1000 element array.

## Run a Series of Tests ##

The shell scripts inside `test` directory allow for creating test
cases and running a series of tests all at once with a single command.

But **make sure** you understand those shell scripts before running
them as they may overwrite your existing input files.  Read the README
file inside `test` directory.