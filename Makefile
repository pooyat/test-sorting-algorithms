
CFLAGS=-g -Wall -std=c++11 #-Werror -O2
CC=g++

all: project5

project5: main.cpp list.o Makefile
	${CC} ${CFLAGS} -o project5 main.cpp list.o

list.o: list.cpp list.h Makefile
	${CC} ${CFLAGS} -o list.o -c list.cpp

clean:
	rm -f *~ *.o project5
